SINGLE_TYPE = 1
LIST_TYPE = 2


class Format:
    def __init__(self, source_title, formatted_title, local_title, value_type, values=None, max_elements=None):
        self.source_title = source_title
        self.formatted_title = formatted_title
        self.local_title = local_title
        self.value_type = value_type
        self.max_elements = max_elements
        self.values = values

    def get_title_value(self, data):
        if self.value_type == SINGLE_TYPE:
            for title, value in data.items():
                if self.source_title == title:
                    formatted_value = int(value) if not self.values else self.values[value]
                    return self.formatted_title, formatted_value

        elif self.value_type == LIST_TYPE:
            result_value = [None] * self.max_elements
            for title, value in data.items():
                if self.source_title in title:
                    item_number = int(title.split("_")[-1])
                    result_value[item_number] = int(value)
            return self.formatted_title, result_value


formats = [
    Format("KRA_ADMIN_100m", "admin", "Маска территории", SINGLE_TYPE, values={
        0: False,
        1: True
    }),

    Format("KRA_LANDCOVER_100m", "landcover", "Землепользование", SINGLE_TYPE, values={
        1: "Акватории",
        2: "Леса",
        3: "Не используется",
        4: "Сезонно затапливаемые земли",
        5: "Сельскохозяйственные поля",
        6: "Не используется",
        7: "Застроенные территории",
        8: "Территории, свободные от растительности",
        9: "Ледники, снежные поля",
        10: "Неизвестно",
        11: "Луга и пастбища",
    }),

    Format("KRA_PREC_100m", "prec", "Среднемесячное количество осадков", LIST_TYPE, max_elements=12),

    Format("KRA_RELIEF_ASPECT_100m", "relief", "Уклон местности", SINGLE_TYPE),
    Format("KRA_RELIEF_HEIGHT_100m", "relief_height", "Высота местности", SINGLE_TYPE),
    Format("KRA_RELIEF_SLOPE_100m", "relief_slope", "Уклон местности", SINGLE_TYPE),

    Format("KRA_SOILTEXTURE_100m", "soiltexture", "Тип почвы", SINGLE_TYPE, values={
        1: "Глина (clay)",
        2: "Пылеватая глина (silty clay)",
        3: "Пылевато-глинистый суглинок (slity clay loam)",
        4: "Опесчаненная глина (sandy clay)",
        5: "Опесчаненный глинистый суглинок (sandy clay loam)",
        6: "Глинистый суглинок (clay loam)",
        7: "Тонкий суглинок (silt)",
        8: "Пылеватый суглинок (silt loam)",
        9: "Суглинок (loam)",
        10: "Песок (sand)",
        11: "Суглинистый песок(loamy sand)",
        12: "Опесчаненный суглинок(sandy loam)",
    }),

    Format("KRA_SUNNY_DAYS_APR_OCT_100m", "sunny_days_apr_oct",
           "Среднее количество солнечных дней в период с апреля по октябрь", SINGLE_TYPE),

    Format("KRA_TAVG_100m", "tavg", "Среднемесячная температура воздуха", LIST_TYPE, max_elements=12),
    Format("KRA_TMIN_100m", "tmin", "Минимальная температура воздуха", LIST_TYPE, max_elements=12),
    Format("KRA_TMAX_100m", "tmax", "Максимальная температура воздуха", LIST_TYPE, max_elements=12),

    Format("KRA_VINEYARDS_100m", "vineyards", "Виноградник", SINGLE_TYPE, values={
        0: False,
        1: True
    }),

    Format("KRA_WATER_SEASONALYTY_100m", "water_seasonality", "Затапливаемость", SINGLE_TYPE),
]


def format_point_info(info):
    new_info = {}
    for form in formats:
        try:
            title, value = form.get_title_value(info)
        except:
            title, value = form.source_title, None
        new_info[title] = value

    return new_info


def get_formatted_title(source_title):
    for form in formats:
        if form.source_title in source_title:
            if len(source_title) > len(form.source_title):
                return form.local_title + " (" + str((int(source_title.split("_")[-1]) + 1)) + " месяц)"
            else:
                return form.local_title
