def parse_plots():
    with open("data/vineyards_info.txt") as f:
        lines = f.read().split("\n")

    titles = lines[0].split("\t")

    parsed_items = []

    for line in lines[1:-1]:
        item = {}
        for i, value in enumerate(line.split("\t")):
            item[titles[i]] = int(float(value))
        parsed_items.append(item)
    return parsed_items


plots = parse_plots()
