from collections import OrderedDict

import numpy as np

seed_value = 42
np.random.seed(seed_value)
import random

random.seed(seed_value)
import os

os.environ['PYTHONHASHSEED'] = str(seed_value)

import numpy as np
from tifffile import imread
import pandas as pd

seed_value = 42
np.random.seed(seed_value)
import random

random.seed(seed_value)
import os

os.environ['PYTHONHASHSEED'] = str(seed_value)

X_left = 310050
Y_lower = 4800050


def getpx(x, y):
    x = (x - X_left) // 100
    y = 4096 - (y - Y_lower) // 100

    return x, y


def get_meters(px_x, px_y):
    x = px_x * 100 + X_left
    y = Y_lower + (4096 - px_y) * 100

    return x, y


def get_info_by_point(x: int, y: int, r=3) -> dict:
    px_x, px_y = getpx(x, y)
    result = dict()

    points = []
    for i in range(px_x - r, px_x + r + 1):
        for j in range(px_y - r, px_y + r + 1):
            points.append([data_img[k][j][i] for k in feature_names])

    points_df = pd.DataFrame(points, columns=feature_names).astype(int).describe()
    for feature_name in feature_names:
        result[feature_name] = points_df[feature_name]["50%"].astype(int)

    return result


def get_distr_by_px(px_x: int, px_y: int, r=3) -> dict:
    result = dict()

    for feature_name in feature_names:
        chunk = data_img[feature_name][px_y - r: px_y + r, px_x - r: px_x + r]
        result[feature_name] = (chunk.mean(), chunk.std())

    return result


def sigma_difference(x_std, y_std, n, m):
    return ((x_std ** 2) / n + (y_std ** 2) / m) ** .5 + 2e-3


def z_value(x_mean, y_mean, x_std, y_std, n, m):
    return abs(x_mean - y_mean) / sigma_difference(x_std, y_std, n, m)


def Grapeness(data1: OrderedDict, n1: int, data2: OrderedDict, n2: int):
    result, _, _ = deviations_from_sample(data1, data2)

    return result


def deviations_from_sample(data1: OrderedDict, data2: OrderedDict, wt=1.5, et=3):
    result = 0
    w = set()
    e = set()
    for feature in reliable_cols:
        x_mean, x_std = data1[feature]
        y_mean, y_std = data2[feature]

        summand = abs(x_mean - y_mean) / (x_mean + 1e-2)
        if summand > et:
            e.add(feature)
        elif summand > wt:
            w.add(feature)

        result += summand

    return result, w, e


def calculate_G(x1, y1, x2=None, y2=None, total=False):
    if not total:
        id = vineyards_img[y1][x1]
        if id > 0:
            n1 = vineyards_area[id]
            data1 = dist_params[id]
        else:
            r = 3
            data1 = get_distr_by_px(x1, y1, r=r)
            n1 = (2 * r + 1) ** 2

        id = vineyards_img[y2][x2]
        if id > 0:
            n2 = vineyards_area[id]
            data2 = dist_params[id]
        else:
            r = 3
            data2 = get_distr_by_px(x2, y2, r=r)
            n2 = (2 * r + 1) ** 2

        return Grapeness(data1, n1, data2, n2)

    else:
        id = vineyards_img[y1][x1]
        penalty = 0
        # if id > 0:
        #     penalty += 20000
        r = 3
        info = get_distr_by_px(x1, y1, r=r)
        n1 = (2 * r + 1) ** 2

        if info['KRA_WATER_SEASONALYTY_100m'][0] > 1:
            penalty += 20000

        # print(info)
        return Grapeness(total_dist, total_n, info, n1) + penalty


def get_w_e(x1, y1, x2=None, y2=None, total=False):
    if not total:
        id = vineyards_img[y1][x1]
        if id > 0:
            n1 = vineyards_area[id]
            data1 = dist_params[id]
        else:
            r = 3
            data1 = get_distr_by_px(x1, y1, r=r)
            n1 = (2 * r + 1) ** 2

        id = vineyards_img[y2][x2]
        if id > 0:
            n2 = vineyards_area[id]
            data2 = dist_params[id]
        else:
            r = 3
            data2 = get_distr_by_px(x2, y2, r=r)
            n2 = (2 * r + 1) ** 2

        g, w, e = deviations_from_sample(data1, data2)
        return w, e

    else:
        id = vineyards_img[y1][x1]
        penalty = 0
        # if id > 0:
        #     penalty += 20000
        r = 3
        info = get_distr_by_px(x1, y1, r=r)
        n1 = (2 * r + 1) ** 2

        if info['KRA_WATER_SEASONALYTY_100m'][0] > 1:
            penalty += 20000

        # print(info)
        g, w, e = deviations_from_sample(total_dist, info)
        return w, e


def launchSwarm(n_iterations, image):
    print("Launch Swarm")
    gbest = Point(0, 0)
    score = -np.inf  # глобальный максимум
    best_boids = set()  # сет боидов, нашедших максимум
    for it in range(n_iterations):
        print(f'Iteration: {it}')
        # найти глобальный максимум
        # curr = get_gbest()
        curr, curr_score = get_gbest()  # new

        # обновить глобальный максимум
        if score <= curr_score:
            score = curr_score
            gbest = curr

        for i in range(len(boids)):
            # обновляем базу данных боида: максимум роя
            boids[i].gbest = gbest
            boids[i].gbest_score = score

            boids[i].score = G(boids[i].pos)
            b_curr_score = boids[i].score  # new
            # обновить базу данных боида: максимум боида
            if boids[i].pbest_score < b_curr_score:
                boids[i].pbest = boids[i].pos  # обновляем координаты локального экстремума
                boids[i].pbest_score = b_curr_score

            # сравнить локальный маскимум боида и роя
            if boids[i].pbest_score > score * 0.85:
                best_boids.add(i + 1)
            else:
                # перемещаем агента
                boids[i] = motion(boids[i])

    print("Number of best_boids:", len(best_boids))


''' Class init '''


# класс описания координат
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __sub__(self, o):
        neg_p = Point(-o.x, -o.y)
        return self.__add__(neg_p)

    def __add__(self, o):
        new_x = self.x + o.x
        new_y = self.y + o.y
        return Point(new_x, new_y)


# класс описания вектора скорости
class Vector:
    def __init__(self, vel_x, vel_y):
        self.x = vel_x
        self.y = vel_y

    def __add__(self, o):
        return Vector(self.x + o.x, self.y + o.y)


# описание и свойства агента - боида
class Boid:
    # pos - координаты в пространстве
    # v - вектор скорости перемещения
    # gbest - координата с лучшим глобальным решением (решение роя)
    # pbest - координата с лучшим локальным решением (решение боида)

    def __init__(self, pos, v=Vector(0, 0)):
        self.pos = pos
        self.v = v
        self.score = -np.inf
        self.pbest = self.pos
        self.pbest_score = -np.inf
        self.gbest = self.pos
        self.gbest_score = -np.inf


''' Swarm init '''


# функция, которую мы оптимизируем (виноградность)
def G(pos):
    # print(pos.x, pos.y)
    return -calculate_G(pos.x, pos.y, total=True)


def get_gbest():
    gbest = Point(0, 0)
    score = -np.inf
    for b in boids:
        if score < G(b.pos):
            score = G(b.pos)
            gbest = b.pos
    return gbest, score


def get_v(X, v, pbest, gbest, a1=1.0, a2=1.0):
    # v - вектор скорости частицы
    # a1, a2 - постоянные ускорения
    # pbest - лучшая найденная частицей точка
    # gbest - лучшая точка из пройденных всеми частицами системы
    # X - текущее положение частицы
    # rnd() - возвращает случайное число [0, 1]
    def rnd():
        return abs(np.random.uniform(0.5, 1))

    r1 = rnd()
    r2 = rnd()
    pbest_ratio = a1 * r1  # вес ностальгии
    gbest_ratio = a2 * r2  # вес роя

    pbest_vec = pbest - X
    gbest_vec = gbest - X
    pbest_vec = Vector(round(pbest_ratio * float(pbest_vec.x)), round(pbest_ratio * float(pbest_vec.y)))
    gbest_vec = Vector(round(gbest_ratio * float(gbest_vec.x)), round(gbest_ratio * float(gbest_vec.y)))

    # return - новое направление и длину вектора скорости частицы
    return v + pbest_vec + gbest_vec


def motion(b):
    V = get_v(b.pos, b.v, b.pbest, b.gbest)  # новый вектор направления боида
    # b.v = V # записываем хз зачем, пока не придумала
    new_pos = b.pos + Point(V.x, V.y)  # новая позиция бота
    # print("  ", "->", new_pos.x, new_pos.y)
    b.pos = new_pos
    b.pos.x = np.clip(b.pos.x, 0, 4095)
    b.pos.y = np.clip(b.pos.y, 0, 4095)
    # assert 1<0
    return b


def convert_to_new_range(oldvalue, oldrange=270 - 1.5, oldmin=1.5, oldmax=500, newrange=10, newmax=0,
                         newmin=-10):
    return -((((oldvalue - oldmin) * newrange) / oldrange) + newmin)


def convert_to_0_10(value):
    if value < 500:
        return convert_to_new_range(value, oldrange=500 - 1.5, oldmin=1.5, oldmax=500, newrange=10, newmax=-3,
                                    newmin=-10)
    else:
        return convert_to_new_range(value, oldrange=350000000 - 400, oldmin=400, oldmax=350000000, newrange=3, newmax=0,
                                    newmin=-3)


def get_best(tr=1.75):
    best_boids = [[convert_to_new_range(-b.score), b.pos] for b in boids if b.score > b.gbest_score * tr]
    print(len(best_boids))
    return best_boids


vineyards_ids = list(range(1, 216 + 1))

data_img = OrderedDict()
feature_names = []
for filename in sorted(os.listdir("data")):
    if '.tif' in filename:

        feature_name = filename.split('.')[0]
        image = imread(f"data/{filename}")

        if len(image.shape) > 2:
            for k in range(image.shape[2]):
                data_img[f"{feature_name}_{k}"] = image[:, :, k]
                feature_names.append(f"{feature_name}_{k}")
        else:
            data_img[feature_name] = image
            feature_names.append(feature_name)

vineyards_img = imread("data/KRA_VINEYARDS_WITH_NUMBERS_100m.tif")

x, y = vineyards_img.shape
vineyards_info = {id: [] for id in vineyards_ids}
for i in range(x):
    for j in range(y):
        id = vineyards_img[j][i]
        if id > 0:
            sample = [data_img[k][j][i] for k in feature_names]
            vineyards_info[id].append(sample)

vineyards_info = {id: np.array(vineyards_info[id]) for id in vineyards_ids}
vineyards_df = {id: pd.DataFrame(vineyards_info[id], columns=[k for k in data_img.keys()]) for id in vineyards_ids}
describe = {id: vineyards_df[id].describe() for id in vineyards_ids}

for col in feature_names:
    for id in vineyards_ids:
        vineyards_df[id][col] = vineyards_df[id][col].apply(
            lambda x: describe[id][col]['50%'] if x < -9999 or x > 65534 else x)

reliable_cols = [c for c in feature_names if c not in ['KRA_SOILTEXTURE_100m',
                                                       'KRA_LANDCOVER_100m',
                                                       'KRA_ADMIN_100m',
                                                       'KRA_WATER_SEASONALYTY_100m',
                                                       'KRA_VINEYARDS_100m',
                                                       'KRA_VINEYARDS_WITH_NUMBERS_100m']]
cat_cols = ['KRA_SOILTEXTURE_100m', 'KRA_LANDCOVER_100m', 'KRA_WATER_SEASONALYTY_100m', 'KRA_VINEYARDS_100m']

dist_params = {id: OrderedDict() for id in vineyards_ids}
for col in reliable_cols:
    for id in vineyards_ids:
        mu, std = vineyards_df[id][col].mean(), vineyards_df[id][col].std()
        dist_params[id][col] = (mu, std)

for col in cat_cols:
    for id in vineyards_ids:
        mode = vineyards_df[id][col].mode()
        dist_params[id][col] = mode

vineyards_area = {id: len(vineyards_info[id]) for id in vineyards_ids}

total_data = []
for id in vineyards_ids:
    total_data += list(np.array(vineyards_df[id]))
total_df = pd.DataFrame(total_data, columns=feature_names)

total_dist = dict()
for feature in reliable_cols:
    total_dist[feature] = (total_df[feature].mean(), total_df[feature].std())

total_n = len(total_df)

n_boids = 169

boids = []
for x in np.linspace(200, 1800, int(n_boids ** .5)):
    for y in np.linspace(1250, 2250, int(n_boids ** .5)):
        boids.append(Boid(Point(int(x), int(y))))
launchSwarm(15, data_img['KRA_SOILTEXTURE_100m'])
