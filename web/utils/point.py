import utm

from .data import calculate_G, convert_to_0_10, getpx, get_w_e
from .format import format_point_info, get_formatted_title


def process_point_info_and_make_human_readable(point_info, lat, lon):
    def formatted_titles(source_titles):
        return [get_formatted_title(title) for title in source_titles]

    new_point_info = format_point_info(point_info)
    new_point_info["lat"] = lat
    new_point_info["lon"] = lon
    xy = convert_from_latlon(lat, lon)
    x, y = xy[0], xy[1]
    x_px, y_px = getpx(x, y)
    g = calculate_G(x_px, y_px, total=True)
    new_point_info["grapeness"] = max(0.1, convert_to_0_10(g))
    w, e = get_w_e(x_px, y_px, total=True)

    new_point_info["warnings"] = formatted_titles(w)
    new_point_info["errors"] = formatted_titles(e)
    return new_point_info


def convert_to_latlon(x, y):
    return utm.to_latlon(int(x), int(y), 37, "S")


def convert_from_latlon(lat, lon):
    result = utm.from_latlon(lat, lon, 37, "S")
    x, y = int(result[0]), int(result[1])
    return x, y
