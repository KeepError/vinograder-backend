from flask import Flask
from flask_cors import CORS

import web.utils.data
from .database import database

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
database = database.Database("sqlite:///app.db?check_same_thread=False")


def create_app():
    """App creation"""

    from . import handlers

    api_url_prefix = "/api"
    app.register_blueprint(handlers.plot.blueprint, url_prefix=api_url_prefix)
    app.register_blueprint(handlers.point.blueprint, url_prefix=api_url_prefix)
    app.register_blueprint(handlers.cadastre.blueprint, url_prefix=api_url_prefix)
    app.register_blueprint(handlers.factories.blueprint, url_prefix=api_url_prefix)

    return app
