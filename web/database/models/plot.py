from sqlalchemy import Column, Integer, String

from ._base import Base


class Plot(Base):
    __tablename__ = "plot"

    id = Column(Integer, primary_key=True)
    lat = Column(String, index=True, nullable=True)
    lon = Column(String, index=True, nullable=True)
