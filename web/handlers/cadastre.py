import requests
import urllib3
from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource
from requests.adapters import HTTPAdapter

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

blueprint = Blueprint(
    "cadastre_resource",
    __name__,
)
api = Api(blueprint)


class CadastreSearchResource(Resource):
    def get(self):
        text = request.args.get("text", "")

        s = requests.Session()
        s.mount('https://', HTTPAdapter(max_retries=100))
        search_result = s.get(
            "https://pkk.rosreestr.ru/api/features/?text={text}&tolerance=4&types=[1]".format(text=text), verify=False)
        search_result_json = search_result.json()

        result = []
        for item in search_result_json["results"]:
            item_id = item["attrs"]["id"]
            cadastre_plot_info = s.get("https://pkk.rosreestr.ru/api/features/1/{item_id}".format(item_id=item_id),
                                       verify=False)
            info = cadastre_plot_info.json()
            info["url"] = f"https://pkk.rosreestr.ru/#/search/{text}/16/@1b4ulz5utt?text={item_id}&type=1&nameTab&indexTab&opened={item_id}"
            result.append(info)

        return jsonify(result)


api.add_resource(CadastreSearchResource, "/cadastre/search")
