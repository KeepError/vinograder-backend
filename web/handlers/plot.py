from flask import Blueprint, jsonify
from flask_restful import Api, Resource

from web.utils.data import get_meters
from web.utils.plots import plots
from web.utils.point import convert_to_latlon, process_point_info_and_make_human_readable

blueprint = Blueprint(
    "plots_resource",
    __name__,
)
api = Api(blueprint)


class PlotResource(Resource):
    def get(self, plot_id):
        for plot in plots:
            if plot["n"] == plot_id:
                lat, lon = convert_to_latlon(plot["x"], plot["y"])
                return jsonify({"info": process_point_info_and_make_human_readable(plot, lat, lon)})
        return "Not found", 404


class PlotListResource(Resource):
    def get(self):
        new_items = []
        for plot in plots:
            new_item = dict()
            new_item["id"] = plot["n"]
            x, y = plot["x"], plot["y"]
            lat, lon = convert_to_latlon(x, y)
            new_item["lat"], new_item["lon"] = lat, lon
            new_items.append(new_item)
        return new_items


class ProspectivePlotListResource(Resource):
    def get(self):
        # items = get_best()
        # result_coords = []
        # for item in items:
        #     x, y = get_meters(item[1].x, item[1].y)
        #     lat, lon = convert_to_latlon(x, y)
        #     result_coords.append({"lat": float(lat), "lon": float(lon)})
        xy_items = [
            (596, 2220),
            (763, 2145),
            (1299, 2451),
            (498, 2064),
            (1305, 2464),
            (505, 1986),
            (1114, 2445),
            (1580, 2455),
            (478, 2040),
            (1760, 2551),
            (1124, 2375),
            (1116, 2449),
            (488, 2050),
            (596, 2147),
            (500, 1976),
            (1400, 2469),
            (1212, 2454),
            (240, 1815),
            (1113, 2445),
            (507, 1895),
            (680, 2290),
            (316, 1824),
            (1750, 2534),
            (1395, 2452),
            (298, 1961),
            (240, 1991),
            (681, 2228),
            (1752, 2535),
            (756, 2133),
            (674, 2216),
            (1491, 2458),
            (242, 2000),
            (587, 2138),
            (671, 2294),
            (671, 2210),
            (1308, 2464),
            (1317, 2454),
            (1482, 2458),
            (756, 2130),
            (663, 2285),
            (510, 1892),
            (1485, 2453),
            (1760, 2556),
            (1499, 2461),
            (1120, 2459),
            (1130, 2372),
            (1690, 2561),
            (486, 2054),
            (1111, 2443),
            (395, 1887),
            (476, 2050),
            (583, 2142),
            (502, 1988),
            (1757, 2541),
            (495, 1991),
            (1475, 2448),
            (315, 1823),
            (1315, 2458),
            (676, 2302)
        ]
        result_items = []
        for xy_item in xy_items:
            lat, lon = convert_to_latlon(*get_meters(*xy_item))
            result_items.append({
                "lat": lat, "lon": lon
            })
        return result_items


api.add_resource(PlotResource, "/plots/<int:plot_id>")
api.add_resource(PlotListResource, "/plots")
api.add_resource(ProspectivePlotListResource, "/plots/prospective")
