from flask import Blueprint, jsonify
from flask_restful import Api, Resource

from web.utils.data import get_info_by_point
from web.utils.point import convert_from_latlon, process_point_info_and_make_human_readable

blueprint = Blueprint(
    "points_resource",
    __name__,
)
api = Api(blueprint)


class PointResource(Resource):
    def get(self, latlon):
        lat, lon = map(float, latlon.split(","))
        point = convert_from_latlon(lat, lon)

        try:
            info = get_info_by_point(int(point[0]), int(point[1]))
            if not info["KRA_ADMIN_100m"] or info["KRA_LANDCOVER_100m"] == 1:
                return "Wrong region", 400
        except (IndexError, KeyError):
            return "Wrong region", 400

        return jsonify({"info": process_point_info_and_make_human_readable(info, lat, lon)})


api.add_resource(PointResource, "/points/<latlon>")
