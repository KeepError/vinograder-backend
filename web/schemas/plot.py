from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

from web.database.models.plot import Plot


class PlotSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Plot
