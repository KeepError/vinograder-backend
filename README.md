# Vinograder (Backend)

## Requirements

Python 3.10

## Deploy

```console
$ pip install requirements.txt
$ python run.py
```